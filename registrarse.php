<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <title>Registro</title>
</head>
<body class="bg-danger">
    <center>
    <div class="container mt-5 pt-5 my-5 col-5 bg-light border border-5 border-dark">
<form action="logica/controlador_crear.php" method= "POST">
<div class="mx-4 col-4">
    <label for="exampleInputEmail1" class="form-label">Identificacion</label>
    <input type="text" name="identificacion" class="form-control" id="identificacion" placeholder="Digite su documento">
  </div>
  <div class="mx-4 col-4">
    <label for="exampleInputEmail1" class="form-label">Usuario</label>
    <input type="text" name="usuario" class="form-control" id="usuario" placeholder="Digite usuario">
  </div>
  <div class="mx-4 col-4">
    <label for="exampleInputEmail1" class="form-label">Correo</label>
    <input type="email" name="correo" class="form-control" id="correo" placeholder="Digite correo">
  </div>
  <div class="mb-3 col-4">
    <label for="exampleInputPassword1" class="form-label">Contraseña</label>
    <input type="password" name="clave" class="form-control" id="clave" placeholder="Digite contraseña">
  </div>
  <button type="submit" class="btn btn-danger my-5 col-5">Registrarse</button>
  </div>
  </center>
</form>
</body>
</html>