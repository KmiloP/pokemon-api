<?php
session_start();
$usuario = $_SESSION['usuario'];

if($usuario == null || $usuario=""){
    header("Location: ../login.php");
    die();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="../imagenes/Smoochum-favicon.png" type="image/x-icon">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <title>Smoochum</title>
</head>
<body class="bg-danger">
    <div class="container bg-light border border-5 border-dark my-5">
        <div class="row">
            <!--Imagen-->
        <div class="col-sm-5">
            <img src="../imagenes/Smoochum.png" width="350">
         </div>
         <!--Informacion-->
         <div class="col-sm-7">
            <h1>En cuanto se le ensucia la cara, aunque sea un poco, se la lava con agua, en el resto del cuerpo, la suciedad no parece importarle mucho.</h1>
            <table class="table table-striped" border="2">
                <tr class="table-danger">
                    <th width="100px">Altura</th>
                    <th width="100px">Categoría</th>
                    <th width="80px">Peso</th>
                    <th width="100px">Habilidad</th>
                    <th width="100px">Sexo</th>
                    <th width="80px">Tipo</th>
                    <th width="80px">Debilidad</th>
                </tr>
                <tr>
                    <td>0,4 Metros</td>
                    <td>Beso</td>
                    <td>6 KG</td>
                    <td>Despiste, Alerta</td>
                    <td>Femenino</td>
                    <td>Hielo, Psíquico</td>
                    <td>Acero, Fantasma, Fuego, Siniestro, Roca, Bicho</td>
                </tr>
            </table>
            <h1>Evoluciones</h1>
            <h2>Jynx</h2>
         </div>
        </div>
    </div>    
</body>
</html>