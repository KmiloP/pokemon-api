<?php
session_start();
$usuario = $_SESSION['usuario'];

if($usuario == null || $usuario=""){
    header("Location: ../login.php");
    die();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="../imagenes/Teddiursa-favicon.png" type="image/x-icon">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <title>Teddiursa</title>
</head>
<body class="bg-danger">
    <div class="container bg-light border border-5 border-dark my-5">
        <div class="row">
            <!--Imagen-->
        <div class="col-sm-5">
            <img src="../imagenes/Teddiursa.png" width="500">
         </div>
         <!--Informacion-->
         <div class="col-sm-7">
            <h1>A Teddiursa le encanta chuparse las palmas impregnadas de dulce miel, este Pokémon fabrica su propia miel mezclando frutos y el polen que recoge Beedrill.</h1>
            <table class="table table-striped" border="2">
                <tr class="table-danger">
                    <th width="100px">Altura</th>
                    <th width="100px">Categoría</th>
                    <th width="80px">Peso</th>
                    <th width="100px">Habilidad</th>
                    <th width="100px">Sexo</th>
                    <th width="80px">Tipo</th>
                    <th width="80px">Debilidad</th>
                </tr>
                <tr>
                    <td>0,6 Metros</td>
                    <td>Osito</td>
                    <td>8,8 KG</td>
                    <td>Recogida, Pies Rápidos</td>
                    <td>Masculino, Femenino</td>
                    <td>Normal</td>
                    <td>Lucha</td>
                </tr>
            </table>
            <h1>Evoluciones</h1>
            <h2>Ursaring, Ursaluna</h2>
         </div>
        </div>
    </div>    
</body>
</html>