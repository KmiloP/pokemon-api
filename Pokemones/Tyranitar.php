<?php
session_start();
$usuario = $_SESSION['usuario'];

if($usuario == null || $usuario=""){
    header("Location: ../login.php");
    die();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="../imagenes/Tyranitar-favicon.png" type="image/x-icon">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <title>Tyranitar</title>
</head>
<body class="bg-danger">
    <div class="container bg-light border border-5 border-dark my-5">
        <div class="row">
            <!--Imagen-->
        <div class="col-sm-5">
            <img src="../imagenes/Tyranitar.png" width="500">
         </div>
         <!--Informacion-->
         <div class="col-sm-7">
            <h1>Casi ningún ataque hace mella en su cuerpo, por lo que le encanta desafiar a sus enemigos.</h1>
            <table class="table table-striped" border="2">
                <tr class="table-danger">
                    <th width="100px">Altura</th>
                    <th width="100px">Categoría</th>
                    <th width="80px">Peso</th>
                    <th width="100px">Habilidad</th>
                    <th width="100px">Sexo</th>
                    <th width="80px">Tipo</th>
                    <th width="80px">Debilidad</th>
                </tr>
                <tr>
                    <td>2 Metros</td>
                    <td>Coraza</td>
                    <td>202 KG</td>
                    <td>Chorro Arena</td>
                    <td>Masculino, Femenino</td>
                    <td>Roca, Siniestro</td>
                    <td>Acero, Lucha, Agua, Hada, Planta, Bicho, Tierra</td>
                </tr>
            </table>
            <h1>Evoluciones</h1>
            <h2>Mega-Tyranitar</h2>
         </div>
        </div>
    </div>    
</body>
</html>